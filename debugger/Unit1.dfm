object Form1: TForm1
  Left = 192
  Top = 107
  Width = 943
  Height = 655
  Caption = 'ThePLang debugger'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object listCode: TListBox
    Left = 16
    Top = 56
    Width = 297
    Height = 553
    ItemHeight = 13
    Items.Strings = (
      '')
    TabOrder = 0
  end
  object listTCode: TListBox
    Left = 328
    Top = 56
    Width = 289
    Height = 553
    ItemHeight = 13
    TabOrder = 1
  end
  object cmdOpen: TButton
    Left = 24
    Top = 16
    Width = 75
    Height = 25
    Caption = '&Open'
    TabOrder = 2
    OnClick = cmdOpenClick
  end
  object cmdStep: TButton
    Left = 232
    Top = 16
    Width = 75
    Height = 25
    Caption = '&Step'
    TabOrder = 3
    Visible = False
    OnClick = cmdStepClick
  end
  object varGrid: TStringGrid
    Left = 632
    Top = 56
    Width = 289
    Height = 553
    ColCount = 3
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing]
    ScrollBars = ssVertical
    TabOrder = 4
    ColWidths = (
      38
      170
      51)
  end
  object openDialog: TOpenDialog
    Left = 136
    Top = 16
  end
  object Timer1: TTimer
    Left = 184
    Top = 16
  end
end
