unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ThePLang_TLB, Grids;

type
  TForm1 = class(TForm)
    listCode: TListBox;
    listTCode: TListBox;
    openDialog: TOpenDialog;
    Timer1: TTimer;
    cmdOpen: TButton;
    cmdStep: TButton;
    varGrid: TStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure cmdOpenClick(Sender: TObject);
    procedure cmdStepClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  pl: PLang;
  Form1: TForm1;

implementation

{$R *.dfm}


function StringToBinaryVariant (Str : string) : Variant;
var
  Buf : PChar;
begin
  Result := VarArrayCreate([0, Length (Str)], varByte);
  Buf := VarArrayLock(Result);
  try
    Move (PChar (Str)^, Buf^, Length (Str));
    PChar(Str)[Length(str)+1] := Chr(0);
  finally
    VarArrayUnlock (Result);
  end;
end;

function BinaryVariantToString (const Value : Variant) : string;
var
  Buf : PChar;
  aLength : Integer;
begin
  if VarIsArray (Value) and ((VarType (Value) and varByte) <> 0) then
  begin
    aLength := VarArrayHighBound(Value, 1) - VarArrayLowBound(Value, 1) + 1;
    Buf := VarArrayLock(Value);
    try
      SetString (Result, Buf, aLength);
    finally
      VarArrayUnlock (Value);
    end;
  end else
    Result := VarToStr (Value);
end;



procedure TForm1.FormCreate(Sender: TObject);
begin
  pl := CoPLang.Create;
end;

procedure TForm1.cmdOpenClick(Sender: TObject);
var
  tmp : PWideChar;
  fname, buf : string;
  ole : OleVariant;

  f : TextFile;
  i : integer;
begin
  openDialog.Filter := 'ThePLang source (*.pas)|*.pas|Any files (*.*)|*.*';

  if openDialog.Execute then
  begin
    fname := openDialog.FileName;
    ole := StringToBinaryVariant(fname);
    pl.openVar(ole);

    AssignFile(f, fname);
    Reset(f);
    listCode.Items.Clear;
    
    while not Eof(f) do
    begin
      Readln(f, buf);
      listCode.Items.Add(buf);
    end;

    CloseFile(f);

    listTCode.Clear;
    for i := 0 to pl.tSize - 1 do
    begin
      listTCode.Items.add(Trim(VarToStr(pl.getTLine(i))));
    end;

    cmdStep.Visible := true;

  end;
end;

procedure TForm1.cmdStepClick(Sender: TObject);
var
  i : integer;
  a, b: integer;
begin
  pl.tick;

  varGrid.RowCount := pl.varSize + 1;
  for i := 0 to pl.varSize - 1 do
  begin
    varGrid.Cells[0, i+1] := IntToStr(i);
    varGrid.Cells[1, i+1] := Trim(VarToStr(pl.getVarName(i)));
    varGrid.Cells[2, i+1] := IntToStr(pl.getVarValue(i));
  end;
  a := pl.getIp;
  b := pl.getLine - 1;
  if a >= 0 then
    listTCode.Selected[a] := true;

  if b >= 0 then
    listCode.Selected[b] := true;
end;

end.
