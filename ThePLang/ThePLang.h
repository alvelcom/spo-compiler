// ThePLang.h : main header file for the ThePLang DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CThePLangApp
// See ThePLang.cpp for the implementation of this class
//

class CThePLangApp : public CWinApp
{
public:
	CThePLangApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

STDAPI Run(const char *filename);