#pragma once

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <iomanip>

// Lexer

enum LexemType
{
	L_UND,

	L_VAR,
	L_ID,
	L_SEMICOLON,
	L_COLON,
	L_COMMA,
	L_BEGIN,
	L_END,
	L_ASSIGN,
	L_EQ,
	L_NEQ,
	L_LT,
	L_LTE,
	L_GT,
	L_GTE,
	L_INT,
	L_ADD,
	L_SUB,
	L_MUL,
	L_DIV,
	L_MOD,
	L_AND,
	L_OR,
	L_XOR,
	L_NOT,
	L_OBRACKET,
	L_CBRACKET,
	L_DOT,

	L_IF,
	L_THEN,
	L_ELSE,
	L_FOR,
	L_TO,
	L_DO,
	L_WHILE,

	L_STR,

};

struct Lexem
{
	typedef LexemType Type;


	LexemType type;
	std::string whole;
	std::string filename;
	int line;

	inline Lexem(const Type &_type, const std::string &_whole, 
		const std::string &_filename, const int &_line)
		: type(_type), whole(_whole), filename(_filename), line(_line)
	{}
};

typedef std::vector<Lexem> Lexems;
typedef Lexems::iterator LexIter;


inline const char *
lexTypeToStr(const Lexem::Type type)
{
#define CASE(name) case name: return #name;

	switch (type)
	{	
		CASE(L_UND)
		CASE(L_VAR)
		CASE(L_ID)
		CASE(L_SEMICOLON)
		CASE(L_COLON)
		CASE(L_COMMA)
		CASE(L_BEGIN)
		CASE(L_END)
		CASE(L_ASSIGN)
		CASE(L_EQ)
		CASE(L_NEQ)
		CASE(L_LT)
		CASE(L_LTE)
		CASE(L_GT)
		CASE(L_GTE)
		CASE(L_INT)
		CASE(L_ADD)
		CASE(L_SUB)
		CASE(L_MUL)
		CASE(L_DIV)
		CASE(L_MOD)
		CASE(L_AND)
		CASE(L_OR)
		CASE(L_XOR)
		CASE(L_NOT)
		CASE(L_OBRACKET)
		CASE(L_CBRACKET)
		CASE(L_DOT)
		CASE(L_IF)
		CASE(L_THEN)
		CASE(L_ELSE)
		CASE(L_FOR)
		CASE(L_TO)
		CASE(L_DO)
		CASE(L_WHILE)
		CASE(L_STR)

	default: return "Bad";
	};

#undef CASE
}

template<typename T, typename T2> inline std::basic_ostream<T, T2> &
operator<<(std::basic_ostream<T, T2> &s, const Lexem &lexem)
{
	s << "Lexem " << lexTypeToStr(lexem.type)
	  << " \t\"" << lexem.whole << "\" \t" << lexem.line;
	return s;
}

template<typename T, typename T2> inline std::basic_ostream<T, T2> &
operator<<(std::basic_ostream<T, T2> &s, const Lexems &lexems)
{
	s << "Lexems [\n";
	for (int i = 0; i < lexems.size(); i++)
		s << "\t" << lexems[i] << ", \n";
	s << "]";
	return s;
}

// Parser
struct P_Program;

struct P_Statement;
struct P_Statements;
struct P_If;
struct P_For;
struct P_While;
struct P_Do;
struct P_Assign;

struct P_Expr;
struct P_Var;
struct P_Num;
struct P_UnarOp;
struct P_BinOp;
struct P_Call;
struct P_Str;

// Operation types
enum UnarOpType
{
	O_COPY,
	O_NOT,
	O_NEGATE
};

enum BinOpType
{
	O_ADD,
	O_SUB,
	O_MUL,
	O_DIV,
	O_MOD,

	O_EQ,
	O_NEQ,
	O_LT,
	O_LTE,
	O_GT,
	O_GTE,

	O_AND,
	O_OR,
	O_XOR
};

// Tree address code
struct T_Context;

struct T_Code;
struct T_Branch;
struct T_Jump;
struct T_Const;
struct T_UnarOp;
struct T_BinOp;
struct T_Call;
struct T_Str;

typedef std::vector<T_Code *> T_List;
typedef unsigned int T_Ptr;

// Some definitions
typedef int Var;
typedef std::string VarName;
struct VarInfo
{
	Var id;
	VarName name;
	std::string type;
	
	// Place of declaration
	std::string filename;
	int line;
};

struct P_Program 
{
	int lastvar;
	std::vector<VarInfo *> vars;
	std::map<VarName, Var> varName;
	P_Statement *code;

	inline Var newVar(const VarName &_n, const std::string &_t, const std::string &_f, int _l)
	{
		VarInfo *vi = new VarInfo;
		vi->filename = _f;
		vi->id = vars.size();
		vi->line = _l;
		vi->name = _n;
		vi->type = _t;
		vars.push_back(vi);
		return varName[_n] = vi->id;
	}

	~P_Program();
};

struct P_Statement
{
	std::string filename;
	int line;

	inline void
	fromLex(const Lexem &lex)
	{
		filename = lex.filename;
		line = lex.line;
	}
		
	virtual void toT(T_Context *context) = 0;
	virtual void toS(std::ostream &s, const P_Program *program, int indent) = 0;

	virtual ~P_Statement();
};

struct P_Expr : P_Statement
{
	virtual void toT(T_Context *context);
	virtual void toT(T_Context *context, const Var &) = 0;
};

struct T_Context
{
	std::vector<VarInfo *> vars;
	std::vector<std::string> strings;

	std::vector<int> memory; // For eval
	T_Ptr ip;
	
	T_List codes;

	inline Var newVar(const VarName &_n, const std::string &_t, const std::string &_f, int _l)
	{
		VarInfo *vi = new VarInfo;
		vi->filename = _f;
		vi->id = vars.size();
		vi->line = _l;
		vi->name = _n;
		vi->type = _t;
		vars.push_back(vi);
		return vi->id;
	}

	inline Var newTmp(const P_Statement *st)
	{
		return newVar("", "integer", st->filename, st->line);
	}

	inline void showVar(std::ostream &s, Var var) const
	{
		s << "%" << (var < 10 ? "0" : "") << var;
	}

	void toS(std::ostream &s) const;
	void toAsm(std::ostream &s) const;

	void doStep();

	~T_Context();
};

struct T_Code
{
	std::string filename;
	int line;

	virtual void toS(std::ostream &s, const T_Context *context) = 0;
	virtual void toAsm(std::ostream &s, const T_Context *context) = 0;
	virtual void eval(T_Context *context) = 0;

	inline void fromStat(const P_Statement *s)
	{
		filename = s->filename;
		line = s->line;
	}

	virtual ~T_Code();
};


// Prototypes
void do_lexer(const char *src, Lexems &dst);
P_Program *do_parser(LexIter &it);
