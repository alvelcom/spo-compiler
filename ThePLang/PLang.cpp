// PLang.cpp : implementation file
//

#include "stdafx.h"
#include "ThePLang.h"
#include "PLang.h"
#include <sstream> 
#include <fstream> 


using namespace std;
// CPLang

IMPLEMENT_DYNCREATE(CPLang, CCmdTarget)


CPLang::CPLang()
{
	EnableAutomation();
	
	// To keep the application running as long as an OLE automation 
	//	object is active, the constructor calls AfxOleLockApp.
	
	AfxOleLockApp();
}

CPLang::~CPLang()
{
	// To terminate the application when all objects created with
	// 	with OLE automation, the destructor calls AfxOleUnlockApp.
	
	AfxOleUnlockApp();

	/*
	if (NULL != this->program)
		delete program;

	if (NULL != this->context)
		delete context;
	*/
}


void CPLang::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(CPLang, CCmdTarget)
END_MESSAGE_MAP()


BEGIN_DISPATCH_MAP(CPLang, CCmdTarget)
	DISP_FUNCTION_ID(CPLang, "open", dispidopen, open, VT_I4, VTS_PI1)
	DISP_FUNCTION_ID(CPLang, "getTLine", dispidgetTLine, getTLine, VT_VARIANT, VTS_I4)
	DISP_FUNCTION_ID(CPLang, "getVarValue", dispidgetVarValue, getVarValue, VT_I4, VTS_I4)
	DISP_PROPERTY_NOTIFY_ID(CPLang, "tSize", dispidtSize, m_tSize, OntSizeChanged, VT_I4)
	DISP_PROPERTY_NOTIFY_ID(CPLang, "varSize", dispidvarSize, m_varSize, OnvarSizeChanged, VT_I4)
	DISP_FUNCTION_ID(CPLang, "getVarName", dispidgetVarName, getVarName, VT_VARIANT, VTS_I4)
	DISP_PROPERTY_NOTIFY_ID(CPLang, "instructionPointer", dispidinstructionPointer, m_instructionPointer, OninstructionPointerChanged, VT_I4)
	DISP_FUNCTION_ID(CPLang, "tick", dispidtick, tick, VT_EMPTY, VTS_NONE)
	DISP_PROPERTY_NOTIFY_ID(CPLang, "line", dispidline, m_line, OnlineChanged, VT_I4)
	DISP_FUNCTION_ID(CPLang, "openVar", dispidopenVar, openVar, VT_EMPTY, VTS_VARIANT)
	DISP_FUNCTION_ID(CPLang, "getIp", dispidgetIp, getIp, VT_I4, VTS_NONE)
	DISP_FUNCTION_ID(CPLang, "getLine", dispidgetLine, getLine, VT_I4, VTS_NONE)
END_DISPATCH_MAP()

// Note: we add support for IID_IPLang to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .IDL file.

// {9913581E-88D3-4DAA-90F8-7DC1CA13BE5E}
static const IID IID_IPLang =
{ 0x9913581E, 0x88D3, 0x4DAA, { 0x90, 0xF8, 0x7D, 0xC1, 0xCA, 0x13, 0xBE, 0x5E } };

BEGIN_INTERFACE_MAP(CPLang, CCmdTarget)
	INTERFACE_PART(CPLang, IID_IPLang, Dispatch)
END_INTERFACE_MAP()

// {AE559301-CB5A-4D13-B723-9C56AFF062AD}
IMPLEMENT_OLECREATE_FLAGS(CPLang, "ThePLang.PLang", afxRegApartmentThreading, 0xae559301, 0xcb5a, 0x4d13, 0xb7, 0x23, 0x9c, 0x56, 0xaf, 0xf0, 0x62, 0xad)


// CPLang message handlers

LONG CPLang::open(CHAR* filename)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	this->filename = filename;
	do_lexer(this->filename.c_str(), this->lexems);

	if (this->lexems.size() < 2)
		return 0;

	this->program = do_parser(lexems.begin());

	this->context = new T_Context;
	this->context->vars = program->vars;

	program->code->toT(this->context);

	std::ofstream asmout("out.asm");
	context->toAsm(asmout);
	asmout.close();

	//system("\\masm32\\bin\\build.bat out");


	m_line = 0;
	m_instructionPointer = context->ip = 0;
	m_tSize = context->codes.size();
	m_varSize = context->vars.size();
	return 0;
}

VARIANT CPLang::getTLine(LONG number)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	VARIANT vaResult;
	VariantInit(&vaResult);

	wchar_t l[100];
	
	if (number < this->context->codes.size())
	{
		std::ostringstream o;
		this->context->codes[number]->toS(o, this->context);
		mbstowcs(l, o.str().c_str(), 100);

		COleVariant ol(l);
		vaResult = ol.Detach();
	}
	else
	{
		COleVariant ol(L"");
		vaResult = ol.Detach();
	}

	return vaResult;
}

LONG CPLang::getVarValue(LONG num)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	return (this->context->memory.size() > num ? context->memory[num] : 0);
}

void CPLang::OntSizeChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: Add your property handler code here
}

void CPLang::OnvarSizeChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: Add your property handler code here
}

VARIANT CPLang::getVarName(LONG num)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	VARIANT vaResult;
	VariantInit(&vaResult);
	

	if (program->vars.size() > num)
	{
		wchar_t l[100];
		mbstowcs(l, program->vars[num]->name.c_str(), 100);
		COleVariant ol(l);
		
		vaResult = ol.Detach();
	}
	else
	{
		COleVariant ol(L"");
		
		vaResult = ol.Detach();
	}

	return vaResult;
}

void CPLang::OninstructionPointerChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: Add your property handler code here
}

void CPLang::tick(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	context->doStep();
	this->m_instructionPointer = context->ip;
	if (context->ip < context->codes.size())
		this->m_line = context->codes[context->ip]->line;

}

void CPLang::OnlineChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: Add your property handler code here
}

void CPLang::openVar(VARIANT &filename)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	//COleVariant v(filename);

	char *buf = (char *)filename.parray->pvData;
	
	this->open(buf);

	// TODO: Add your dispatch handler code here
}

LONG CPLang::getIp(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: Add your dispatch handler code here

	return this->context->ip;
}

LONG CPLang::getLine(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: Add your dispatch handler code here

	if (context->ip < context->codes.size())
		return context->codes[context->ip]->line;

	return 0;
}
