{ This is some test file }
var
  i, j, t : integer;
  theVeryLong : integer; 
  buf : integer;
begin
  buf := "                                            ";
  i := 0;
  while 3 >= i do
  begin
    wsprintf(buf, "%d", i);
    MessageBox(0, buf, "ThePLang compiler", 0);
    i := i + 1;
  end;
  
  MessageBox(0, "=", "ThePLang compiler", 0);
  
  for i := 2 to 19 do
  begin
    t := 0;
    for j := 2 to i - 1 do
      if i mod j = 0 then
        t := 1;
    if t = 0 then
    begin
      wsprintf(buf, "The %d is prime", i);
      MessageBox(0, buf, "ThePLang code", 0);
    end;
  end; 
end.
