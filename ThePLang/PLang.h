
#pragma once

#include "compiler.h"

// CPLang command target

class CPLang : public CCmdTarget
{
	DECLARE_DYNCREATE(CPLang)

public:
	CPLang();
	virtual ~CPLang();

	virtual void OnFinalRelease();

protected:

	std::string filename;
	Lexems lexems;
	P_Program *program;
	T_Context *context;

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CPLang)
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
	LONG open(CHAR* filename);

	enum 
	{
		dispidgetLine = 12L,
		dispidgetIp = 11L,
		dispidopenVar = 10L,
		dispidline = 9,
		dispidtick = 8L,
		dispidinstructionPointer = 7,
		dispidgetVarName = 6L,
		dispidvarSize = 5,
		dispidtSize = 4,
		dispidgetVarValue = 3L,
		dispidgetTLine = 2L,
		dispidopen = 1L
	};
	VARIANT getTLine(LONG number);
	LONG getVarValue(LONG num);
	void OntSizeChanged(void);
	LONG m_tSize;
	void OnvarSizeChanged(void);
	LONG m_varSize;
	VARIANT getVarName(LONG num);
	void OninstructionPointerChanged(void);
	LONG m_instructionPointer;
	void tick(void);
	void OnlineChanged(void);
	LONG m_line;
	void openVar(VARIANT &filename);
	LONG getIp(void);
	LONG getLine(void);
};


