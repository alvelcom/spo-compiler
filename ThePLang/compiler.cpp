#include "stdafx.h"
#include "compiler.h"
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <stdarg.h>

using namespace std;
#define pb push_back

#define forn(i, n) for (int i = 0; i < (n); ++i)
#define forv(i, v) for (unsigned int i = 0; i < (v).size(); ++i)

static bool lex_break_symbols[256] = { 0 };

inline const char *
indenter(int indent)
{
#define T1 "  "
#define T2 T1 T1
#define T4 T2 T2
#define T6 T4 T2
#define T8 T4 T4
#define T10 T8 T2
#define T12 T8 T4
#define T14 T8 T6

	static char *lst[] = {"",
		T1, T2, T2 T1, T4, T4 T1, T6, T6 T1, T8, T8 T1, T10,
		T10 T1, T12, T12 T1, T14, T14 T1, T8 T8
	};
	return lst[indent];
}

const char *unarOpTypeToStr(UnarOpType type)
{
	switch (type)
	{
	case O_COPY: return "";
	case O_NOT: return "not";
	case O_NEGATE: return "-";
	}
	return "<?>";
}

const char *binOpTypeToStr(BinOpType type)
{
#define CASE(type, str) case type: return str
	switch (type)
	{
		CASE(O_ADD, "+");
		CASE(O_SUB, "-");
		CASE(O_MUL, "*");
		CASE(O_DIV, "div");
		CASE(O_MOD, "mod");
		CASE(O_EQ, "=");
		CASE(O_NEQ, "/=");
		CASE(O_LT, "<");
		CASE(O_LTE, "<=");
		CASE(O_GT, ">");
		CASE(O_GTE, ">=");
		CASE(O_AND, "and");
		CASE(O_OR, "or");
		CASE(O_XOR, "xor");
	}
#undef CASE
	return "<?>";
}

static void 
init_lex_break_symbols()
{
	bool *l = lex_break_symbols;
	l['(']  = true;
	l[')']  = true;
	l[']']  = true;
	l[':']  = true;
	l[';']  = true;
	l[',']  = true;
	l['{']  = true;
	l['+']  = true;
	l['-']  = true;
	l['*']  = true;
	l['/']  = true;
	l['<']  = true;
	l['>']  = true; 
	l['"']  = true;
}

void 
do_lexer(const char *src, Lexems &dst)
{
	string filename(src);
	ifstream file(src);

	if (!file.is_open())
		return;

	int line = 1;
	char bufc = 0, last;
	bool in_comment = false;
	bool in_string = false;
	string bufs, str;
	int bufs_line = 1;

	init_lex_break_symbols();

	enum {
		LE_UND, LE_NUM, LE_STR, LE_OP
	} state, newstate;
	state = newstate = LE_UND;

	dst.clear();
	while (!file.eof())
	{
		last = bufc;
		file.get(bufc);
		//file >> bufc;

		if ('\n' == bufc)
			line++;

		if (in_string)
		{
			if ('"' == bufc) 
			{
				in_string = false;
				dst.pb(Lexem(L_STR, str, filename, line));
			}
			else
				str.pb(bufc);
			continue;
		}

		if (in_comment)
		{
			if ('}' == bufc) in_comment = false;
			continue;
		}

		if (isalpha(bufc))
			newstate = LE_STR;
		else if (isalnum(bufc))
			newstate = LE_NUM;
		else if (!isspace(bufc))
			newstate = LE_OP;
		else
			newstate = LE_UND;

		if (isspace(bufc) || lex_break_symbols[bufc]
			|| state != newstate)
		{
			Lexem lex(L_UND, bufs, filename, bufs_line);
			string word;
			word.resize(bufs.size(), 0);
			std::transform(bufs.begin(), bufs.end(),word.begin(), tolower);

#define IF(str, ttype) if ((str) == word) {lex.type = (ttype);} else 
			IF("", L_UND)
			IF("var", L_VAR)
			IF("begin", L_BEGIN)
			IF("end", L_END)
			IF("if", L_IF)
			IF("then", L_THEN)
			IF("else", L_ELSE)
			IF("for", L_FOR)
			IF("to", L_TO)
			IF("do", L_DO)
			IF("while", L_WHILE)
			IF("<", L_LT)
			IF(">", L_GT)
			IF("=", L_EQ)
			IF(":", L_COLON)
			IF("div", L_DIV)
			IF("mod", L_MOD)
			if (isalpha(word[0])) lex.type = L_ID; else
			if (isalnum(word[0])) lex.type = L_INT; else
			{
				lex.type = L_UND;
				printf("Bad lexem '%s' at line %d in file '%s' (next char = '%c')\n", 
					word.c_str(), line, filename.c_str(), bufc);
			}
#undef IF
			if (lex.type != L_UND) 
				dst.pb(lex);
			bufs.clear();
			bufs_line = line;
		}

		if ('{' == bufc)
		{
			in_comment = true;
			state = LE_UND;
			continue;
		}
		if ('"' == bufc)
		{
			in_string = true;
			str = "";
			state = LE_UND;
			continue;
		}
		if (isspace(bufc))
			continue;

		state = newstate;
		bufs.pb(bufc);
		if (isalnum(bufc)) continue;

		Lexem lex(L_UND, bufs, filename, bufs_line);
#define IF(str, ttype) if ((str) == bufs) {lex.type = (ttype);} else
		IF("(", L_OBRACKET)
		IF(")", L_CBRACKET)
		IF(":=", L_ASSIGN)
		IF("/=", L_NEQ)
		IF("<=", L_LTE)
		IF(">=", L_GTE)
		IF("<>", L_NEQ)
		IF("+", L_ADD)
		IF("-", L_SUB)
		IF("*", L_MUL)
		IF(";", L_SEMICOLON)
		IF(",", L_COMMA)
		IF(".", L_DOT)
			lex.type = L_UND;
#undef IF
		if (L_UND != lex.type)
		{
			dst.pb(lex);
			bufs.clear();
			bufs_line = line;
		}
	}

	if (in_comment)
		printf("Warning: Unclosed comment\n");
}

/////////////////////
// Three address code
/////////////////////

void T_Context::toAsm(ostream &s) const
{
	s << ".586" << endl;

	s << ".model flat,stdcall" << endl;
	s << "option casemap:none" << endl;
	s << "include \\masm32\\include\\windows.inc" << endl;
	s << "include \\masm32\\include\\kernel32.inc" << endl;

	s << "includelib \\masm32\\lib\\kernel32.lib" << endl;
	s << "include \\masm32\\include\\user32.inc" << endl;
	s << "includelib \\masm32\\lib\\user32.lib" << endl;

	s << ".data" << endl;
	
	forv(i, this->vars)
		s << "  v" << i << " \tdd 0" << endl;

	forv(i, this->strings)
		s << "  str" << i << "\tdb \"" << this->strings[i] << "\", 0" << endl;

	s << endl;
	s << ".code" << endl;
	s << "start:" << endl;
	
	s << endl << "; ====== START OF CODE ======" << endl;

	forv(i, this->codes)
	{
		s << "lab" << i << ": " << endl;
		s << ";; " << setw(2) << codes[i]->line << "." << setw(4) << i << ":";
		this->codes[i]->toS(s, this);
		this->codes[i]->toAsm(s, this);
		s << endl;
	}

	s << endl << "; ====== END OF CODE ======" << endl;
	s << "lab" << (0 + this->codes.size()) << ": " << endl;
	s << "lab" << (1 + this->codes.size()) << ": " << endl;

	s << "invoke ExitProcess, NULL" << endl;
	s << "end start" << endl;
}

void T_Context::toS(std::ostream &s) const
{
	forv(i, codes)
	{
		s << setw(2) << codes[i]->line << "." << setw(4) << i << ":";
		codes[i]->toS(s, this);
	}
}

void T_Context::doStep()
{
	T_Ptr eval = this->ip;
	this->ip++;

	static bool init = false;
	if (!init)
	{
		init = true;
		this->memory.resize(this->vars.size(), 0);
	}

	if (eval < 0 || eval >= this->codes.size())
		return;

	this->codes[eval]->eval(this);
}

T_Context::~T_Context()
{}

T_Code::~T_Code()
{}

struct T_Branch : T_Code
{
	Var var;
	T_Ptr to;

	void toS(ostream &s, const T_Context *context)
	{
		s << "    bra ";
		context->showVar(s, var);
		s << ", #" << to << endl;
	}

	void toAsm(ostream &s, const T_Context *context)
	{
		s << "  cmp v" << var << ", 0" << endl;
		s << "  je  lab" << to << endl;
	}

	void eval(T_Context *context)
	{
		if (0 == context->memory[var])
			context->ip = to;
	}

	~T_Branch()
	{}
};

struct T_Jump : T_Code
{
	T_Ptr to;

	void toS(ostream &s, const T_Context *context)
	{
		s << "    jmp #" << to << endl;
	}

	void toAsm(ostream &s, const T_Context *context)
	{
		s << "  jmp lab" << to << endl;
	}

	void eval(T_Context *context)
	{
		context->ip = to;
	}

	~T_Jump()
	{}
};

struct T_Const : T_Code
{
	Var dst;
	int num;

	void toS(ostream &s, const T_Context *context)
	{
		s << "    ";
		context->showVar(s, dst);
		s << " <- " << num << endl;
	}

	void toAsm(ostream &s, const T_Context *context)
	{
		s << "  mov v" << dst << ", " << num << endl;
	}

	void eval(T_Context *context)
	{
		context->memory[dst] = num;
	}

	~T_Const()
	{}
};


struct T_UnarOp : T_Code
{
	UnarOpType type;
	Var dst, src;

	void toS(ostream &s, const T_Context *context)
	{
		s << "    ";
		context->showVar(s, dst);
		s << " <- " << unarOpTypeToStr(type) << " ";
		context->showVar(s, src);
		s << endl;
	}

	void toAsm(ostream &s, const T_Context *context)
	{
		s << "  mov eax, v" << src << endl;
		switch (type)
		{
		case O_NEGATE:
			s << "  neg eax" << endl;
			break;
		case O_NOT:
			s << "  cmp eax, 0" << endl;
			s << "  lahf" << endl;
			s << "  shr eax, 14 ; Got ZF" << endl;
			s << "  and eax, 1" << endl;
			break;
		}
		s << "  mov v" << dst << ", eax" << endl;
	}

	void eval(T_Context *context)
	{
		switch(type)
		{
		case O_NEGATE:
			context->memory[dst] = -context->memory[src];
			break;
		case O_NOT:
			context->memory[dst] = !context->memory[src];
			break;
		case O_COPY:
			context->memory[dst] = context->memory[src];
		}
	}
	~T_UnarOp()
	{}
};

struct T_BinOp : T_Code
{
	Var dst, left, right;
	BinOpType type;

	void toS(ostream &s, const T_Context *context)
	{
		s << "    ";
		context->showVar(s, dst);
		s << " <- ";
		context->showVar(s, left);
		s << " " << binOpTypeToStr(type) << " ";
		context->showVar(s, right);
		s << endl;
	}

	void toAsm(ostream &s, const T_Context *context)
	{
		s << "  mov eax, v" << left << endl;
		s << "  mov ebx, v" << right << endl;
		switch (type)
		{
		case O_ADD: 
			s << "  add eax, ebx" << endl;
			break;
		case O_SUB:
			s << "  sub eax, ebx" << endl;
			break;
		case O_MUL:
			s << "  mul ebx" << endl;
			break;
		case O_MOD:
		case O_DIV:
			s << "  mov edx, 0" << endl;
			s << "  div ebx" << endl;
			if (O_MOD == type)
				s << "  mov eax, edx" << endl;
			break;
		case O_AND:
			s << "  and eax, ebx" << endl;
			break;
		case O_OR:
			s << "  or  eax, ebx" << endl;
			break;
		case O_XOR:
			s << "  xor eax, ebx" << endl;
			break;
		default: // comparations
			s << "  cmp eax, ebx" << endl;
			s << "  lahf" << endl;
			// now in eax bit #15 is eq bit
			// now in eax bit #16 is sign bit. 1 is for negative, or less
			switch (type)
			{
			case O_GT: // ok
				// !#15 and !#16
				s << "  shr eax, 14" << endl;
				s << "  and eax, 3; eax & 11b" << endl;
				s << "  mov ebx, eax" << endl;
				s << "  shr ebx, 1" << endl;
				s << "  xor ebx, 1" << endl;
				s << "  xor eax, 1" << endl;
				s << "  and eax, ebx" << endl;
				// sorry for this trash
				break;
			case O_GTE: // ok
				// !16
				s << "  shr eax, 15" << endl;
				s << "  and eax, 1" << endl;
				s << "  xor eax, 1" << endl;
				break;
			case O_EQ: // ok
				s << "  shr eax, 14" << endl;
				s << "  and eax, 1" << endl;
				break;
			case O_NEQ: // ok
				s << "  shr eax, 14" << endl;
				s << "  and eax, 1" << endl;
				s << "  xor eax, 1" << endl;
				break;
			case O_LT: // ok
				s << "  shr eax, 15" << endl;
				s << "  and eax, 1" << endl;
				break;
			case O_LTE: // ok
				s << "  shr eax, 14" << endl;
				s << "  and eax, 3" << endl;
				s << "  mov ebx, eax" << endl;
				s << "  shr ebx, 1" << endl;
				s << "  or eax, ebx" << endl;
				break;
			}
		}
		s << "  mov v" << dst << ", eax" << endl;
	}

	void eval(T_Context *context)
	{
		int l, r, d;
		l = context->memory[left];
		r = context->memory[right];

		switch (type)
		{
		case O_ADD: d = l + r; break;
		case O_SUB: d = l - r; break;
		case O_MUL: d = l * r; break;
		case O_DIV: d = l / r; break;
		case O_MOD: d = l % r; break;

		case O_EQ:  d = l == r; break;
		case O_NEQ: d = l != r; break;
		case O_LT:  d = l <  r; break;
		case O_LTE: d = l <= r; break;
		case O_GT:  d = l >  r; break;
		case O_GTE: d = l >= r; break;

		case O_AND: d = l & r; break;
		case O_OR:  d = l | r; break;
		case O_XOR: d = l ^ r; break;
		}
		context->memory[dst] = d;
	}
	~T_BinOp()
	{}
};

struct T_Call : T_Code
{
	Var dst;
	string name;
	vector<Var> params;

	void toS(ostream &s, const T_Context *context)
	{
		s << "    ";
		context->showVar(s, dst);
		s << " <- invoke " << name << " with ";
		forv(i, params)
		{
			context->showVar(s, params[i]);
			s << " ";
		}
		s << endl;
	}

	void toAsm(ostream &s, const T_Context *context)
	{
		s << "  invoke " << name;
		forv(i, params)
			s << ", v" << params[i];
		s << endl;
		s << "  mov v" << dst << ", eax" << endl;
	}

	void eval(T_Context *context)
	{
		const size_t p = params.size();
		const vector<int> &m = context->memory;
		if (4 == p && "MessageBox" == name)
		{
			MessageBoxA((HWND)m[params[0]],
				(LPCSTR) context->memory[params[1]],
				(LPCSTR) context->memory[params[2]],
				context->memory[params[3]]);
		} 
		else if (2 <= params.size() && "wsprintf" == name)
		{
			switch(p)
			{
			case 2: sprintf((char *)m[params[0]], (char *)m[params[1]]); break;
			case 3: sprintf((char *)m[params[0]], (char *)m[params[1]], m[params[2]]); break;
			case 4: sprintf((char *)m[params[0]], (char *)m[params[1]], m[params[2]], m[params[3]]); break;
			case 5: sprintf((char *)m[params[0]], (char *)m[params[1]], m[params[2]], m[params[3]], m[params[4]]); break;
			case 6: sprintf((char *)m[params[0]], (char *)m[params[1]], m[params[2]], m[params[3]], m[params[4]], m[params[5]]); break;
			}			
		}

	}

	~T_Call()
	{}
};


struct T_Str : T_Code
{
	Var dst;
	int id;

	void toS(ostream &s, const T_Context *context)
	{
		s << "    ";
		context->showVar(s, dst);
		s << " <- \"" << context->strings[id] << "\"" << endl;
	}

	void toAsm(ostream &s, const T_Context *context)
	{
		s << "  lea eax, str" << id << endl;
		s << "  mov v" << dst << ", eax" << endl;

	}

	void eval(T_Context *context)
	{
		context->memory[dst] = reinterpret_cast<int>(context->strings[id].c_str());
	}


	~T_Str()
	{}
};

/////////////////////
// AST def
/////////////////////

P_Program::~P_Program()
{}

P_Statement::~P_Statement()
{}

void P_Expr::toT(T_Context *context) 
{
	toT(context, context->newTmp(this));
}

struct P_Statements : P_Statement
{
	vector<P_Statement *> list;

	void toT(T_Context *context)
	{
		forv(i, list)
			list[i]->toT(context);
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << "begin\n";
		forv(i, list)
		{
			list[i]->toS(s, program, ind+1);
			s << ";\n";
		}
		s << indenter(ind) << "end";
	}

	~P_Statements()
	{
		forv(i, list)
			delete list[i];
	}
};

struct P_If : P_Statement
{
	P_Expr *cond;
	P_Statement *thenBranch, *elseBranch;

	void toT(T_Context *context)
	{
		Var condRes = context->newTmp(this);
		cond->toT(context, condRes);
		
		T_Branch *br = new T_Branch;
		context->codes.pb(br);
		br->fromStat(this);
		br->var = condRes;

		thenBranch->toT(context);
		T_Jump *jmp = new T_Jump;
		jmp->fromStat(this);
		context->codes.pb(jmp);

		br->to = context->codes.size();

		elseBranch->toT(context);

		jmp->to = context->codes.size();
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << "if ";
		cond->toS(s, program, 0);
		s << " then\n";
		thenBranch->toS(s, program, ind+1);
		s << "\n" << indenter(ind) << "else\n";
		elseBranch->toS(s, program, ind+1);
	}
	
	~P_If()
	{
		delete cond;
		delete thenBranch;
		delete elseBranch;
	}
};

struct P_For : P_Statement
{
	Var it;
	P_Expr *from, *to;
	P_Statement *body;

	void toT(T_Context *context)
	{
		Var one = context->newTmp(this);
		Var tt = context->newTmp(this);
		Var cmp = context->newTmp(this);
		from->toT(context, it);
		to->toT(context, tt);
	
		
		T_Const *oneC = new T_Const;
		oneC->fromStat(this);
		oneC->dst = one;
		oneC->num = 1;
		context->codes.pb(oneC);


		T_Ptr start = context->codes.size();
		T_BinOp *op = new T_BinOp;
		op->fromStat(this);
		op->type = O_LTE;
		op->dst = cmp;
		op->left = it;
		op->right = tt;
		context->codes.pb(op);
		
		
		T_Branch *br = new T_Branch;
		br->fromStat(this);
		br->var = cmp;
		context->codes.pb(br);

		body->toT(context);

		T_BinOp *inc = new T_BinOp;
		inc->fromStat(this);
		inc->dst = it;
		inc->left = it;
		inc->right = one;
		inc->type = O_ADD;
		context->codes.pb(inc);

		T_Jump *jmp = new T_Jump;
		jmp->fromStat(this);
		jmp->to = start;
		context->codes.pb(jmp);

		br->to = context->codes.size();
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << "for " << program->vars[it]->name << " := ";
		from->toS(s, program, 0);
		s << " to ";
		to->toS(s, program, 0);
		s << " do\n";
		body->toS(s, program, ind+1);
	}

	~P_For()
	{
		delete from;
		delete to;
		delete body;
	}
};

struct P_While : P_Statement
{
	P_Expr *cond;
	P_Statement *body;

	void toT(T_Context *context)
	{
		T_Ptr start = context->codes.size();
		Var c = context->newTmp(this);

		cond->toT(context, c);

		T_Branch *br = new T_Branch;
		br->fromStat(this);
		br->var = c;
		context->codes.pb(br);

		body->toT(context);

		T_Jump *jmp = new T_Jump;
		jmp->fromStat(this);
		jmp->to = start;
		context->codes.pb(jmp);

		br->to = context->codes.size();
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << "while ";
		cond->toS(s, program, 0);
		s << " do\n";
		body->toS(s, program, ind+1);
	}

	~P_While()
	{
		delete cond;
		delete body;
	}
};

struct P_Do : P_Statement
{
	P_Statement *body;
	P_Expr *cond;

	void toT(T_Context *context)
	{
		T_Ptr start = context->codes.size();
		Var c = context->newTmp(this);

		body->toT(context);
		cond->toT(context, c);

		T_Branch *br = new T_Branch;
		br->fromStat(this);
		br->var = c;
		br->to = context->codes.size() + 2;
		context->codes.pb(br);

		T_Jump *jmp = new T_Jump;
		jmp->fromStat(this);
		jmp->to = start;
		context->codes.pb(jmp);
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << "do\n";
		body->toS(s, program, ind+1);
		s << "\n" << indenter(ind) << "while ";
		cond->toS(s, program, 0);
	}

	~P_Do()
	{
		delete body;
		delete cond;
	}
};

struct P_Assign : P_Statement
{
	Var var;
	P_Expr *expr;

	void toT(T_Context *context)
	{
		expr->toT(context, var);
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << program->vars[var]->name << " := ";
		expr->toS(s, program, 0);
	}

	~P_Assign()
	{
		delete expr;
	}
};

struct P_Var : P_Expr
{
	Var var;
	void toT(T_Context *context, const Var &dst)
	{
		T_UnarOp *op = new T_UnarOp;
		op->fromStat(this);
		op->dst = dst;
		op->src = var;
		op->type = O_COPY;
		context->codes.pb(op);
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << program->vars[var]->name;
	}
};

struct P_Num : P_Expr
{
	int num;
	void toT(T_Context *context, const Var &dst)
	{
		T_Const *c = new T_Const;
		c->fromStat(this);
		c->dst = dst;
		c->num = num;
		context->codes.pb(c);
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << num;
	}
};

struct P_Str : P_Expr
{
	string str;

	void toT(T_Context *context, const Var &dst)
	{
		context->strings.pb(str);
		T_Str *s = new T_Str;
		s->fromStat(this);
		s->dst = dst;
		s->id = context->strings.size() - 1;
		context->codes.pb(s);
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << "\"" << str << "\"";
	}
};

struct P_UnarOp : P_Expr
{
	UnarOpType type;
	P_Expr *expr;

	void toT(T_Context *context, const Var &dst)
	{
		expr->toT(context, dst);

		T_UnarOp *op = new T_UnarOp;
		op->fromStat(this);
		op->dst = dst;
		op->src = dst;
		op->type = type;
		context->codes.pb(op);

	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << unarOpTypeToStr(type) << " (";
		expr->toS(s, program, 0);
		s << ")";
	}

	~P_UnarOp()
	{
		delete expr;
	}
};

struct P_BinOp : P_Expr
{
	BinOpType type;
	P_Expr *left, *right;

	void toT(T_Context *context, const Var &dst)
	{
		Var rr = context->newTmp(this);
		left->toT(context, dst);
		right->toT(context, rr);
		
		T_BinOp *op = new T_BinOp;
		op->fromStat(this);
		op->dst = dst;
		op->left = dst;
		op->right = rr;
		op->type = type;
		context->codes.pb(op);
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << "(";
		left->toS(s, program, 0);
		s << " " << binOpTypeToStr(type) << " ";
		right->toS(s, program, 0);
		s << ")";
	}

	~P_BinOp()
	{
		delete left;
		delete right;
	}
};

struct P_Call : P_Expr
{
	string name;
	vector<P_Expr *> params;

	void toT(T_Context *context, const Var &dst)
	{
		vector<Var> vars(params.size());
		forv(i, params)
			vars[i] = context->newTmp(this);
		forv(i, params)
			params[i]->toT(context, vars[i]);

		T_Call *call = new T_Call;
		call->fromStat(this);
		call->name = name;
		call->params = vars;
		call->dst = dst;
		context->codes.pb(call);
	}

	void toS(ostream &s, const P_Program *program, int ind)
	{
		s << indenter(ind) << name << "(";
		forv(i, params)
		{
			params[i]->toS(s, program, 0);
			if (i+1 != params.size())
				s << ", ";
		}
		s << ")";
	}

	~P_Call()
	{
		forv(i, params)
			delete params[i];
	}
};


///////////////////////////////////////////////////
// Parser
///////////////////////////////////////////////////

// forward declarations
P_Statement *p_statement(LexIter &it, P_Program *program);
P_Expr		*p_expr(LexIter &it, P_Program *program);

P_Statement *p_statements(LexIter &it, P_Program *program);
P_Statement *p_if(LexIter &it, P_Program *program);
P_Statement *p_while(LexIter &it, P_Program *program);
P_Statement *p_for(LexIter &it, P_Program *program);
P_Statement *p_do(LexIter &it, P_Program *program);
P_Statement *p_assign(LexIter &it, P_Program *program);

// and or xor
// > >= = <= <
// + -
// * div mod
// not - ()
// var num
P_Expr *p_logic(LexIter &it, P_Program *program);
P_Expr *p_cmp(LexIter &it, P_Program *program);
P_Expr *p_sum(LexIter &it, P_Program *program);
P_Expr *p_mul(LexIter &it, P_Program *program);
P_Expr *p_token(LexIter &it, P_Program *program);

#define ERR(str) cout << "Parser error: " << (str) << " (got " << *it << ")" << endl
#define EXP(ttype, str) if ((ttype) != it->type) {ERR("Expecting " #ttype ", " str );}
#define EXPr(ttype, str) if ((ttype) != it->type) {ERR("Expecting " #ttype ", " str ); return 0;}

P_Program *
do_parser(LexIter &it)
{
	P_Program *program = new P_Program;

	// read vars
	if (L_VAR == it->type)
	{
		++it;
		do {
			string type = "<error>";
			vector<string> vars;

			do
			{	
				EXP(L_ID, "variable name")
				else
					vars.pb(it->whole);
				++it;
				if (L_COMMA == it->type)
					++it;
				else 
					break;
			} while (L_ID == it->type);

			EXP(L_COLON, "colon")
			else
				++it;

			EXP(L_ID, "typename")
			else
			{
				type = it->whole;
				++it;
				forv(i, vars)
					program->newVar(vars[i], type, it->filename, it->line);
			}

			EXP(L_SEMICOLON, "semicolon")
			++it;
		} while (L_BEGIN != it->type);
	}

	program->code = p_statement(it, program);
	return program;
}

P_Statement *p_statement(LexIter &it, P_Program *program)
{
	if (L_BEGIN == it->type)
		return p_statements(it, program);
	if (L_IF == it->type)
		return p_if(it, program);
	if (L_FOR == it->type)
		return p_for(it, program);
	if (L_WHILE == it->type)
		return p_while(it, program);
	if (L_DO == it->type)
		return p_do(it, program);
	if (L_ID == it->type && L_ASSIGN == (it+1)->type)
		return p_assign(it, program);

	return p_expr(it, program);
}

P_Statement *p_statements(LexIter &it, P_Program *program)
{
	EXPr(L_BEGIN, "begin")

	P_Statements *s = new P_Statements;
	s->fromLex(*it++);

	while (L_END != it->type)
	{
		s->list.pb(p_statement(it, program));
		
		// restore before semicolon
		while (L_SEMICOLON != it->type && L_END != it->type) ++it;
		
		EXP(L_SEMICOLON, "semicolon")
		else
			++it;
	}
	++it;
	return s;
}

P_Statement *p_if(LexIter &it, P_Program *program)
{
	EXPr(L_IF, "if")

	P_If *s = new P_If;
	s->fromLex(*it++);

	s->cond = p_expr(it, program);
	EXPr(L_THEN, "if-THEN")
	++it;

	s->thenBranch = p_statement(it, program);
	if (L_ELSE == it->type)
	{
		++it;
		s->elseBranch = p_statement(it, program);
	}
	else
	{
		s->elseBranch = new P_Statements;
		s->elseBranch->fromLex(*it);
	}

	return s;
}

P_Statement *p_while(LexIter &it, P_Program *program)
{
	EXPr(L_WHILE, "while")

	P_While *s = new P_While;
	s->fromLex(*it++);

	s->cond = p_expr(it, program);
	EXPr(L_DO, "do");
	++it;

	s->body = p_statement(it, program);

	return s;
}

P_Statement *p_for(LexIter &it, P_Program *program)
{
	EXPr(L_FOR, "for")

	P_For *s = new P_For;
	s->fromLex(*it++);

	EXPr(L_ID, "variable")
	s->it = program->varName[it->whole];
	++it;

	EXPr(L_ASSIGN, ":=")
	++it;

	s->from = p_expr(it, program);

	EXPr(L_TO, "to")
	++it;

	s->to = p_expr(it, program);

	EXPr(L_DO, "do")
	++it;

	s->body = p_statement(it, program);

	return s;
}

P_Statement *p_do(LexIter &it, P_Program *program)
{
	EXPr(L_DO, "do");
	
	P_Do *s = new P_Do;
	s->fromLex(*it++);

	s->body = p_statement(it, program);
	
	EXPr(L_WHILE, "while")
	++it;

	s->cond = p_expr(it, program);

	return s;
}

P_Statement *p_assign(LexIter &it, P_Program *program)
{
	EXPr(L_ID, "variable")

	P_Assign *s = new P_Assign;
	s->var = program->varName[it->whole];
	++it;

	EXPr(L_ASSIGN, ":=")
	++it;

	s->expr = p_expr(it, program);

	return s;
}

P_Expr *p_expr(LexIter &it, P_Program *program)
{
	return p_logic(it, program);
}

// and or xor
// > >= = <= <
// + -
// * div mod
// not - ()
// var num

P_Expr *p_logic(LexIter &it, P_Program *program)
{
	P_Expr *left = p_cmp(it, program);
	
	while (L_AND == it->type 
		|| L_OR == it->type
		|| L_XOR == it->type)
	{
		Lexem lex = *it++;
		P_Expr *right = p_cmp(it, program);
		
		P_BinOp *op = new P_BinOp;
		op->fromLex(lex);
		op->left = left;
		op->right = right;
		switch (lex.type)
		{
		case L_AND: op->type = O_AND; break;
		case L_OR:  op->type = O_OR;  break;
		case L_XOR: op->type = O_XOR; break;
		}
		left = op;
	}
	return left;
}

P_Expr *p_cmp(LexIter &it, P_Program *program)
{
	P_Expr *left = p_sum(it, program);
	BinOpType type;
	switch (it->type)
	{
	case L_GT:  type = O_GT; break;
	case L_GTE: type = O_GTE; break;
	case L_EQ:  type = O_EQ; break;
	case L_LTE: type = O_LTE; break;
	case L_LT:  type = O_LT; break;
	case L_NEQ: type = O_NEQ; break;
	default: return left;
	}

	P_BinOp *op = new P_BinOp;
	op->fromLex(*it++);

	op->left = left;
	op->right = p_sum(it, program);
	op->type = type;
	return op;
}

P_Expr *p_sum(LexIter &it, P_Program *program)
{
	P_Expr *left = p_mul(it, program);
	while (L_ADD == it->type
		|| L_SUB == it->type)
	{
		Lexem lex = *it++;
		P_Expr *right = p_mul(it, program);

		P_BinOp *op = new P_BinOp;
		op->fromLex(lex);
		op->left = left;
		op->right = right;
		switch(lex.type)
		{
		case L_ADD: op->type = O_ADD; break;
		case L_SUB: op->type = O_SUB; break;
		}
		left = op;
	}
	return left;
}

P_Expr *p_mul(LexIter &it, P_Program *program)
{
	P_Expr *left = p_token(it, program);
	while (L_MUL == it->type
		|| L_DIV == it->type
		|| L_MOD == it->type)
	{
		Lexem lex = *it++;
		P_Expr *right = p_token(it, program);

		P_BinOp *op = new P_BinOp;
		op->fromLex(lex);
		op->left = left;
		op->right = right;
		switch(lex.type)
		{
		case L_MUL: op->type = O_MUL; break;
		case L_DIV: op->type = O_DIV; break;
		case L_MOD: op->type = O_MOD; break;
		}
		left = op;
	}
	return left;
}

P_Expr *p_token(LexIter &it, P_Program *program)
{
	P_Var *var;
	P_Num *num;
	P_Expr *expr;
	P_Str *str;

	if (L_ID == it->type && L_OBRACKET == (it+1)->type)
	{
		P_Call *call = new P_Call;
		call->fromLex(*it);
		call->name = it->whole;
		++it;
		++it;
		while (true)
		{
			call->params.push_back(0);
			call->params.back() = p_expr(it, program);
			if (L_COMMA == it->type)
			{
				it++;
				continue;
			}
			EXP(L_CBRACKET, ")")
			it++;
			break;
		}
		return call;
	}

	switch (it->type)
	{
	case L_ID:
		var = new P_Var;
		var->fromLex(*it);
		if (program->varName.find(it->whole) == program->varName.end())
		{
			ERR("Bad variable name");
			return 0;
		}
		var->var = program->varName[it->whole];
		++it;
		return var;
	case L_INT:
		num = new P_Num;
		num->fromLex(*it);
		num->num = atoi(it->whole.c_str());
		++it;
		return num;
	case L_STR:
		str = new P_Str;
		str->fromLex(*it);
		str->str = it->whole;
		++it;
		return str;
	case L_OBRACKET:
		expr = p_expr(++it, program);
		EXP(L_CBRACKET, ")")
		++it;
		return expr;
	}

	P_UnarOp *op = new P_UnarOp;
	op->fromLex(*it);
	
	switch (it->type)
	{
	case L_ADD:
		return p_token(++it, program);
	case L_NOT:
		op->type = O_NOT;
		op->expr = p_token(++it, program);	
		return op;
	case L_SUB:
		op->type = O_NEGATE;
		op->expr = p_token(++it, program);
		return op;
	}
	ERR("Something goes wrong");
	return 0;
}
