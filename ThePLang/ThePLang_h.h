

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0500 */
/* at Sat May 11 19:20:29 2013
 */
/* Compiler settings for .\ThePLang.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __ThePLang_h_h__
#define __ThePLang_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IPLang_FWD_DEFINED__
#define __IPLang_FWD_DEFINED__
typedef interface IPLang IPLang;
#endif 	/* __IPLang_FWD_DEFINED__ */


#ifndef __PLang_FWD_DEFINED__
#define __PLang_FWD_DEFINED__

#ifdef __cplusplus
typedef class PLang PLang;
#else
typedef struct PLang PLang;
#endif /* __cplusplus */

#endif 	/* __PLang_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __ThePLang_LIBRARY_DEFINED__
#define __ThePLang_LIBRARY_DEFINED__

/* library ThePLang */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_ThePLang;

#ifndef __IPLang_DISPINTERFACE_DEFINED__
#define __IPLang_DISPINTERFACE_DEFINED__

/* dispinterface IPLang */
/* [uuid] */ 


EXTERN_C const IID DIID_IPLang;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("9913581E-88D3-4DAA-90F8-7DC1CA13BE5E")
    IPLang : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct IPLangVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IPLang * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IPLang * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IPLang * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IPLang * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IPLang * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IPLang * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IPLang * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IPLangVtbl;

    interface IPLang
    {
        CONST_VTBL struct IPLangVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPLang_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IPLang_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IPLang_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IPLang_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IPLang_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IPLang_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IPLang_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* __IPLang_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_PLang;

#ifdef __cplusplus

class DECLSPEC_UUID("AE559301-CB5A-4D13-B723-9C56AFF062AD")
PLang;
#endif
#endif /* __ThePLang_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


