// ThePLang.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "ThePLang.h"

#include <windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>

#include "compiler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CThePLangApp

BEGIN_MESSAGE_MAP(CThePLangApp, CWinApp)
END_MESSAGE_MAP()


// CThePLangApp construction

CThePLangApp::CThePLangApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CThePLangApp object

CThePLangApp theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0x91BA7F3A, 0x46C6, 0x4A10, { 0x91, 0xD9, 0x50, 0xDE, 0x2F, 0xAD, 0x38, 0x55 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;


// CThePLangApp initialization

BOOL CThePLangApp::InitInstance()
{
	CWinApp::InitInstance();

	// Register all OLE server (factories) as running.  This enables the
	//  OLE libraries to create objects from other applications.
	COleObjectFactory::RegisterAll();

	return TRUE;
}

// DllGetClassObject - Returns class factory

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return AfxDllGetClassObject(rclsid, riid, ppv);
}


// DllCanUnloadNow - Allows COM to unload DLL

STDAPI DllCanUnloadNow(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return AfxDllCanUnloadNow();
}


// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return SELFREG_E_TYPELIB;

	if (!COleObjectFactory::UpdateRegistryAll())
		return SELFREG_E_CLASS;

	return S_OK;
}


// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return SELFREG_E_TYPELIB;

	if (!COleObjectFactory::UpdateRegistryAll(FALSE))
		return SELFREG_E_CLASS;

	return S_OK;
}


////////////////////////
// Mine here
////////////////////////

static const WORD MAX_CONSOLE_LINES = 500;
void RedirectIOToConsole(bool parent = false)
{
	int hConHandle;
	long lStdHandle;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	FILE *fp;
	// allocate a console for this app
	if (parent)
		AttachConsole(ATTACH_PARENT_PROCESS);
	AllocConsole();
	// set the screen buffer to be big enough to let us scroll text
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),
		&coninfo);
	coninfo.dwSize.Y = MAX_CONSOLE_LINES;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE),
		coninfo.dwSize);
	// redirect unbuffered STDOUT to the console
	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen( hConHandle, "w" );
	*stdout = *fp;
	setvbuf( stdout, NULL, _IONBF, 0 );
	// redirect unbuffered STDIN to the console
	lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen( hConHandle, "r" );
	*stdin = *fp;
	setvbuf( stdin, NULL, _IONBF, 0 );
	// redirect unbuffered STDERR to the console
	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen( hConHandle, "w" );
	*stderr = *fp;
	setvbuf( stderr, NULL, _IONBF, 0 );
	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog
	// point to console as well
	std::ios::sync_with_stdio();
}

STDAPI Run(const char *filename)
{	
	using namespace std;
	RedirectIOToConsole(true);
	//Sleep(10000);

	filename = "..\\ThePLang\\test.pas";
	
	printf("\nThePLang/0.1\nFile: %s\n", filename);

	Lexems lexems;
	do_lexer(filename, lexems);
	cout << lexems << endl << "ok, parser now" << endl;

	P_Program *program = do_parser(lexems.begin());
	program->code->toS(cout, program, 0);

	T_Context *cont = new T_Context;
	cont->vars = program->vars;

	program->code->toT(cont);

	cout << endl << "ok, tree address code now" << endl;
	cont->toS(cout);
	cout << endl;

	cout << endl << "ok, asm now!" << endl;
	cont->toAsm(cout);
	cout << endl;

	std::ofstream asmout("out.asm");
	cont->toAsm(asmout);
	asmout.close();
	
	system("\\masm32\\bin\\build.bat out");
	return S_OK;
}